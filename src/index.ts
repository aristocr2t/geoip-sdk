import axios, { AxiosInstance } from 'axios';

import { GeoipData, ResponseData } from './typings';

export * from './typings';

export default class GeoipSDK {
	private readonly $http: AxiosInstance = axios.create();

	constructor(private readonly host: string = 'http://localhost:3000') {}

	async get(): Promise<GeoipData | null> {
		try {
			const res = await this.$http.get<ResponseData<GeoipData>>(`${this.host}/api/geoip`);
			return res.data?.item;
		} catch {
			return null;
		}
	}
}
