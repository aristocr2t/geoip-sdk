export interface ResponseData<T extends {} = {}> {
	statusCode: number;
	message: string | null;
	item: T;
}

export interface ListResponseData<T extends {} = {}> {
	statusCode: number;
	message: string | null;
	count: number;
	items: T[];
}

export interface GeoipData {
	ip: string;
	country_code: string;
	country_name: string;
	region_code: string;
	region_name: string;
	city: string;
	zip_code: string;
	time_zone: string;
	latitude: number;
	longitude: number;
	metro_code: number;
}
