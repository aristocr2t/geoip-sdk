import GeoipSDK from '@test/sdk';

console.warn(`You should start @test/server in dev mode!`);

describe('index', () => {
	it('should be done', async done => {
		const $geoip = new GeoipSDK();
		const item = await $geoip.get();
		expect(item.ip).toBe('127.0.0.1');
		done();
	});
});
